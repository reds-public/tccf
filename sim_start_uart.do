vmap work hdl_uart/work
vmap uvvm_util UVVM_All/uvvm_util/sim/uvvm_util/
vmap avalon_mm_bfm UVVM_All/bitvis_vip_avalon_mm/sim/bitvis_vip_avalon_mm/

vsim -t 1ns fss_uart_top

add wave -position insertpoint  \
sim:/fss_uart_top/avalon_mm_config \
sim:/fss_uart_top/GC_ADDR_WIDTH \
sim:/fss_uart_top/GC_DATA_WIDTH \
sim:/fss_uart_top/UART_ADDR_WIDTH \
sim:/fss_uart_top/UART_DATA_WIDTH \
sim:/fss_uart_top/clk_s \
sim:/fss_uart_top/uart1_s \
sim:/fss_uart_top/uart2_s

add wave -divider -height 10 UART_A
add wave -position insertpoint  \
sim:/fss_uart_top/A_ack_s \
sim:/fss_uart_top/A_addr_s \
sim:/fss_uart_top/A_addr_uart_s \
sim:/fss_uart_top/A_avalon_mm_master_if \
sim:/fss_uart_top/A_byte_en_s \
sim:/fss_uart_top/A_cts_s \
sim:/fss_uart_top/A_cyc_s \
sim:/fss_uart_top/A_data_read_s \
sim:/fss_uart_top/A_data_write_s \
sim:/fss_uart_top/A_uart_write_s \
sim:/fss_uart_top/A_dcd_s \
sim:/fss_uart_top/A_dsr_s \
sim:/fss_uart_top/A_dtr_s \
sim:/fss_uart_top/A_irq_s \
sim:/fss_uart_top/A_rd_en_s \
sim:/fss_uart_top/A_rd_ready_s \
sim:/fss_uart_top/A_reset_s \
sim:/fss_uart_top/A_resetn_s \
sim:/fss_uart_top/A_ri_s \
sim:/fss_uart_top/A_rts_s \
sim:/fss_uart_top/A_sel_reg \
sim:/fss_uart_top/A_sel_s \
sim:/fss_uart_top/A_stb_s \
sim:/fss_uart_top/A_waitrequest_n \
sim:/fss_uart_top/A_wr_en_s \
sim:/fss_uart_top/A_wr_ready_s

add wave -divider -height 10 UART_B \
sim:/fss_uart_top/B_ack_s \
sim:/fss_uart_top/B_addr_s \
sim:/fss_uart_top/B_addr_uart_s \
sim:/fss_uart_top/B_avalon_mm_master_if \
sim:/fss_uart_top/B_byte_en_s \
sim:/fss_uart_top/B_cts_s \
sim:/fss_uart_top/B_cyc_s \
sim:/fss_uart_top/B_data_read_s \
sim:/fss_uart_top/B_data_write_s \
sim:/fss_uart_top/B_uart_write_s \
sim:/fss_uart_top/B_dcd_s \
sim:/fss_uart_top/B_dsr_s \
sim:/fss_uart_top/B_dtr_s \
sim:/fss_uart_top/B_irq_s \
sim:/fss_uart_top/B_rd_en_s \
sim:/fss_uart_top/B_rd_ready_s \
sim:/fss_uart_top/B_reset_s \
sim:/fss_uart_top/B_resetn_s \
sim:/fss_uart_top/B_ri_s \
sim:/fss_uart_top/B_rts_s \
sim:/fss_uart_top/B_sel_reg \
sim:/fss_uart_top/B_sel_s \
sim:/fss_uart_top/B_stb_s \
sim:/fss_uart_top/B_waitrequest_n \
sim:/fss_uart_top/B_wr_en_s \
sim:/fss_uart_top/B_wr_ready_s

add wave -divider -height 10 REGS_A
add wave -position insertpoint  \
sim:/fss_uart_top/uart_A/regs/block_cnt \
sim:/fss_uart_top/uart_A/regs/block_value \
sim:/fss_uart_top/uart_A/regs/clk \
sim:/fss_uart_top/uart_A/regs/counter_t \
sim:/fss_uart_top/uart_A/regs/cts \
sim:/fss_uart_top/uart_A/regs/cts_c \
sim:/fss_uart_top/uart_A/regs/cts_pad_i \
sim:/fss_uart_top/uart_A/regs/dcd \
sim:/fss_uart_top/uart_A/regs/dcd_c \
sim:/fss_uart_top/uart_A/regs/dcd_pad_i \
sim:/fss_uart_top/uart_A/regs/delayed_modem_signals \
sim:/fss_uart_top/uart_A/regs/dl \
sim:/fss_uart_top/uart_A/regs/dlab \
sim:/fss_uart_top/uart_A/regs/dlc \
sim:/fss_uart_top/uart_A/regs/dsr \
sim:/fss_uart_top/uart_A/regs/dsr_c \
sim:/fss_uart_top/uart_A/regs/dsr_pad_i \
sim:/fss_uart_top/uart_A/regs/dtr_pad_o \
sim:/fss_uart_top/uart_A/regs/enable \
sim:/fss_uart_top/uart_A/regs/fcr \
sim:/fss_uart_top/uart_A/regs/fifo_read \
sim:/fss_uart_top/uart_A/regs/fifo_write \
sim:/fss_uart_top/uart_A/regs/ier \
sim:/fss_uart_top/uart_A/regs/iir \
sim:/fss_uart_top/uart_A/regs/iir_read \
sim:/fss_uart_top/uart_A/regs/int_o \
sim:/fss_uart_top/uart_A/regs/lcr \
sim:/fss_uart_top/uart_A/regs/loopback \
sim:/fss_uart_top/uart_A/regs/lsr \
sim:/fss_uart_top/uart_A/regs/lsr0 \
sim:/fss_uart_top/uart_A/regs/lsr0_d \
sim:/fss_uart_top/uart_A/regs/lsr0r \
sim:/fss_uart_top/uart_A/regs/lsr1 \
sim:/fss_uart_top/uart_A/regs/lsr1_d \
sim:/fss_uart_top/uart_A/regs/lsr1r \
sim:/fss_uart_top/uart_A/regs/lsr2 \
sim:/fss_uart_top/uart_A/regs/lsr2_d \
sim:/fss_uart_top/uart_A/regs/lsr2r \
sim:/fss_uart_top/uart_A/regs/lsr3 \
sim:/fss_uart_top/uart_A/regs/lsr3_d \
sim:/fss_uart_top/uart_A/regs/lsr3r \
sim:/fss_uart_top/uart_A/regs/lsr4 \
sim:/fss_uart_top/uart_A/regs/lsr4_d \
sim:/fss_uart_top/uart_A/regs/lsr4r \
sim:/fss_uart_top/uart_A/regs/lsr5 \
sim:/fss_uart_top/uart_A/regs/lsr5_d \
sim:/fss_uart_top/uart_A/regs/lsr5r \
sim:/fss_uart_top/uart_A/regs/lsr6 \
sim:/fss_uart_top/uart_A/regs/lsr6_d \
sim:/fss_uart_top/uart_A/regs/lsr6r \
sim:/fss_uart_top/uart_A/regs/lsr7 \
sim:/fss_uart_top/uart_A/regs/lsr7_d \
sim:/fss_uart_top/uart_A/regs/lsr7r \
sim:/fss_uart_top/uart_A/regs/lsr_mask \
sim:/fss_uart_top/uart_A/regs/lsr_mask_condition \
sim:/fss_uart_top/uart_A/regs/lsr_mask_d \
sim:/fss_uart_top/uart_A/regs/mcr \
sim:/fss_uart_top/uart_A/regs/modem_inputs \
sim:/fss_uart_top/uart_A/regs/ms_int \
sim:/fss_uart_top/uart_A/regs/ms_int_d \
sim:/fss_uart_top/uart_A/regs/ms_int_pnd \
sim:/fss_uart_top/uart_A/regs/ms_int_rise \
sim:/fss_uart_top/uart_A/regs/msi_reset \
sim:/fss_uart_top/uart_A/regs/msr \
sim:/fss_uart_top/uart_A/regs/msr_read \
sim:/fss_uart_top/uart_A/regs/rda_int \
sim:/fss_uart_top/uart_A/regs/rda_int_d \
sim:/fss_uart_top/uart_A/regs/rda_int_pnd \
sim:/fss_uart_top/uart_A/regs/rda_int_rise \
sim:/fss_uart_top/uart_A/regs/rf_count \
sim:/fss_uart_top/uart_A/regs/rf_data_out \
sim:/fss_uart_top/uart_A/regs/rf_error_bit \
sim:/fss_uart_top/uart_A/regs/rf_overrun \
sim:/fss_uart_top/uart_A/regs/rf_pop \
sim:/fss_uart_top/uart_A/regs/rf_push_pulse \
sim:/fss_uart_top/uart_A/regs/ri \
sim:/fss_uart_top/uart_A/regs/ri_c \
sim:/fss_uart_top/uart_A/regs/ri_pad_i \
sim:/fss_uart_top/uart_A/regs/rls_int \
sim:/fss_uart_top/uart_A/regs/rls_int_d \
sim:/fss_uart_top/uart_A/regs/rls_int_pnd \
sim:/fss_uart_top/uart_A/regs/rls_int_rise \
sim:/fss_uart_top/uart_A/regs/rstate \
sim:/fss_uart_top/uart_A/regs/rts_pad_o \
sim:/fss_uart_top/uart_A/regs/rx_reset \
sim:/fss_uart_top/uart_A/regs/scratch \
sim:/fss_uart_top/uart_A/regs/serial_in \
sim:/fss_uart_top/uart_A/regs/serial_out \
sim:/fss_uart_top/uart_A/regs/srx_pad \
sim:/fss_uart_top/uart_A/regs/srx_pad_i \
sim:/fss_uart_top/uart_A/regs/start_dlc \
sim:/fss_uart_top/uart_A/regs/stx_pad_o \
sim:/fss_uart_top/uart_A/regs/tf_count \
sim:/fss_uart_top/uart_A/regs/tf_push \
sim:/fss_uart_top/uart_A/regs/thre_int \
sim:/fss_uart_top/uart_A/regs/thre_int_d \
sim:/fss_uart_top/uart_A/regs/thre_int_pnd \
sim:/fss_uart_top/uart_A/regs/thre_int_rise \
sim:/fss_uart_top/uart_A/regs/thre_set_en \
sim:/fss_uart_top/uart_A/regs/ti_int \
sim:/fss_uart_top/uart_A/regs/ti_int_d \
sim:/fss_uart_top/uart_A/regs/ti_int_pnd \
sim:/fss_uart_top/uart_A/regs/ti_int_rise \
sim:/fss_uart_top/uart_A/regs/trigger_level \
sim:/fss_uart_top/uart_A/regs/tstate \
sim:/fss_uart_top/uart_A/regs/tx_reset \
sim:/fss_uart_top/uart_A/regs/wb_addr_i \
sim:/fss_uart_top/uart_A/regs/wb_dat_i \
sim:/fss_uart_top/uart_A/regs/wb_dat_o \
sim:/fss_uart_top/uart_A/regs/wb_re_i \
sim:/fss_uart_top/uart_A/regs/wb_rst_i \
sim:/fss_uart_top/uart_A/regs/wb_we_i

add wave -divider -height 10 AVALON_A
add wave -position insertpoint  \
sim:/fss_uart_top/uart_A/wb_interface/clk \
sim:/fss_uart_top/uart_A/wb_interface/re_o \
sim:/fss_uart_top/uart_A/wb_interface/wb_ack_o \
sim:/fss_uart_top/uart_A/wb_interface/wb_adr_i \
sim:/fss_uart_top/uart_A/wb_interface/wb_adr_int \
sim:/fss_uart_top/uart_A/wb_interface/wb_adr_int_lsb \
sim:/fss_uart_top/uart_A/wb_interface/wb_adr_is \
sim:/fss_uart_top/uart_A/wb_interface/wb_cyc_i \
sim:/fss_uart_top/uart_A/wb_interface/wb_cyc_is \
sim:/fss_uart_top/uart_A/wb_interface/wb_dat8_i \
sim:/fss_uart_top/uart_A/wb_interface/wb_dat8_o \
sim:/fss_uart_top/uart_A/wb_interface/wb_dat32_o \
sim:/fss_uart_top/uart_A/wb_interface/wb_dat_i \
sim:/fss_uart_top/uart_A/wb_interface/wb_dat_is \
sim:/fss_uart_top/uart_A/wb_interface/wb_dat_o \
sim:/fss_uart_top/uart_A/wb_interface/wb_rst_i \
sim:/fss_uart_top/uart_A/wb_interface/wb_sel_i \
sim:/fss_uart_top/uart_A/wb_interface/wb_sel_is \
sim:/fss_uart_top/uart_A/wb_interface/wb_stb_i \
sim:/fss_uart_top/uart_A/wb_interface/wb_stb_is \
sim:/fss_uart_top/uart_A/wb_interface/wb_we_i \
sim:/fss_uart_top/uart_A/wb_interface/wb_we_is \
sim:/fss_uart_top/uart_A/wb_interface/wbstate \
sim:/fss_uart_top/uart_A/wb_interface/we_o \
sim:/fss_uart_top/uart_A/wb_interface/wre

run -all
