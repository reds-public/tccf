#!/bin/bash
# FSS serial port demo - build script
#
# FSS project - REDS Institute, HEIG-VD, Yverdon-les-Bains (CH)
# Alberto Dassatti, Anthony Convers, Roberto Rigamonti, Xavier Ruppen -- 02.2018

# UVVM compilation
cd ../UVVM_All/bitvis_vip_avalon_mm/script
vsim -c -64 -do "do compile_uvvm.do; quit"
vsim -c -64 -do "do compile_bfm.do; quit"
cd ../../../hdl_uart

# UVVM mapping
vmap uvvm_util ../UVVM_All/uvvm_util/sim/uvvm_util/
vmap avalon_mm_bfm ../UVVM_All/bitvis_vip_avalon_mm/sim/bitvis_vip_avalon_mm/

# FSS compilation
vlib work
vlog +define+LITLE_ENDIAN uart16550/trunk/rtl/verilog/*.v +incdir+./uart16550/trunk/rtl/verilog

vcom -2008 fli_socket.vhd
vcom -2008 clock.vhd
vcom -2008 fss_uart_top.vhd
